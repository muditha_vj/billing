package com.zone24x7.training.billing.db.entity;

import com.zone24x7.training.billing.Bill;
import com.zone24x7.training.billing.enumeration.BillingType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillingTest {

    @Test
    public void return_correctBillTotal_when_cdrDetailsAreGiven() {
        //Init params
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY,17);
        cal.set(Calendar.MINUTE,30);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        Date date = cal.getTime();

        CDR cdr=new CDR();
        cdr.setDurationInSeconds(60);
        cdr.setStartTime(date);
        cdr.setOriginatingPhoneNumber(new PhoneNumber("0714413469"));
        cdr.setReceivedPhoneNumber(new PhoneNumber("0714413469"));

        CallPackage callPackage= new CallPackage();
        callPackage.setPeakHourStart(LocalTime.of(8,0));
        callPackage.setOffPeakHourStart(LocalTime.of(20,0));
        callPackage.setRental(100.0);
        callPackage.setBillingType(BillingType.PER_MINUTE);

        CustomerPhone customerPhone= new CustomerPhone();
        customerPhone.setPackage(callPackage);
        customerPhone.setPhoneNumber(new PhoneNumber("0714413469"));

        CallCharge callCharge= new CallCharge();
        callCharge.setPeak_Charge(3.0);
        callCharge.setOffPeak_Charge(3.0);
        callCharge.setCallType("Local Calls");

        List<CallCharge> list= new ArrayList<>();
        list.add(callCharge);

        Bill bill= new Bill();
        bill.setCallCharges(list);

        assertEquals(bill.getFinalBillAmount(), 1000);
    }

}
