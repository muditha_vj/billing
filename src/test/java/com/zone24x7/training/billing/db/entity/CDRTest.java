package com.zone24x7.training.billing.db.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CDRTest {

	@Test
	public void return_true_when_phoneNumberExtIsLocal() {
			CDR cdr=new CDR();
			cdr.setOriginatingPhoneNumber(new PhoneNumber("0714413469"));
			cdr.setReceivedPhoneNumber(new PhoneNumber("0714413469"));
			boolean isLocal = cdr.isLocal();
			assertEquals(isLocal, true);
	}

	@Test
	public void return_false_when_phoneNumberExtIsNotLocal() {
		CDR cdr=new CDR();
		cdr.setOriginatingPhoneNumber(new PhoneNumber("0724413469"));
		cdr.setReceivedPhoneNumber(new PhoneNumber("0734413469"));
		boolean isLocal = cdr.isLocal();
		assertEquals(isLocal, false);
	}

}
