package com.zone24x7.training.billing.enumeration;

public enum BillingType {
    PER_MINUTE,
    PER_SECOND;
}
