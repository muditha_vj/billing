package com.zone24x7.training.billing;

import com.zone24x7.training.billing.db.entity.CallCharge;

import java.util.List;

public class Bill {

    private List<CallCharge> callCharges;
    private double monthlyRental;
    private List<Tax> taxes;
    private double finalBillAmount;

    public List<CallCharge> getCallCharges() {
        return callCharges;
    }

    public void setCallCharges(List<CallCharge> callCharges) {
        this.callCharges = callCharges;
    }

    public double getMonthlyRental() {
        return monthlyRental;
    }

    public void setMonthlyRental(double monthlyRental) {
        this.monthlyRental = monthlyRental;
    }

    public List<Tax> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<Tax> taxes) {
        this.taxes = taxes;
    }

    public double getFinalBillAmount() {
        return finalBillAmount;
    }

    public void setFinalBillAmount(double finalBillAmount) {
        this.finalBillAmount = finalBillAmount;
    }
}
