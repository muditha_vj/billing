package com.zone24x7.training.billing;

public class GovernmentTax implements Tax {
    private double taxRate;

    public GovernmentTax(double taxRate) {
        this.taxRate = taxRate;
    }

    @Override
    public double calculateTax(double billAmount) {
        return billAmount * taxRate;
    }
}
