package com.zone24x7.training.billing.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CallCharge {

    @Id
    private int Id;
    public double Peak_Charge;
    public double OffPeak_Charge;
    public String callType;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getPeak_Charge() {
        return Peak_Charge;
    }

    public void setPeak_Charge(double peak_Charge) {
        Peak_Charge = peak_Charge;
    }

    public double getOffPeak_Charge() {
        return OffPeak_Charge;
    }

    public void setOffPeak_Charge(double offPeak_Charge) {
        OffPeak_Charge = offPeak_Charge;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }
}
