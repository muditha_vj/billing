package com.zone24x7.training.billing.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDate;

/**
 * Created by TharinduMP on 11/12/2018.
 */
@Entity
public class CustomerPhone {

    @Id
    private int id;
    @OneToOne
    private PhoneNumber phoneNumber;
    @OneToOne
    private CallPackage Package;
    private LocalDate registeredDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CallPackage getPackage() {
        return Package;
    }

    public void setPackage(CallPackage aPackage) {
        Package = aPackage;
    }

    public LocalDate getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(LocalDate registeredDate) {
        this.registeredDate = registeredDate;
    }
}
