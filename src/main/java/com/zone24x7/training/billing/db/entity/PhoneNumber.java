package com.zone24x7.training.billing.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PhoneNumber {

    @Id
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public PhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExtension() {
        return this.phoneNumber.substring(0,3);
    }
}
