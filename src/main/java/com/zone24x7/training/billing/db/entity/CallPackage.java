package com.zone24x7.training.billing.db.entity;

import com.zone24x7.training.billing.enumeration.BillingType;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalTime;

@Entity
public class CallPackage {
    @Id
    private Integer id;
    private Double rental;
    private BillingType billingType;
    private LocalTime peakHourStart;
    private LocalTime offPeakHourStart;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getRental() {
        return rental;
    }

    public void setRental(Double rental) {
        this.rental = rental;
    }

    public BillingType getBillingType() {
        return billingType;
    }

    public void setBillingType(BillingType billingType) {
        this.billingType = billingType;
    }

    public LocalTime getPeakHourStart() {
        return peakHourStart;
    }

    public void setPeakHourStart(LocalTime peakHourStart) {
        this.peakHourStart = peakHourStart;
    }

    public LocalTime getOffPeakHourStart() {
        return offPeakHourStart;
    }

    public void setOffPeakHourStart(LocalTime offPeakHourStart) {
        this.offPeakHourStart = offPeakHourStart;
    }
}
