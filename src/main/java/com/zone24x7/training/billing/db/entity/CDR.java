package com.zone24x7.training.billing.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class CDR {

    @Id
    private int id;
    @ManyToOne
    private PhoneNumber originatingPhoneNumber;
    @ManyToOne
    private PhoneNumber receivedPhoneNumber;
    private Date startTime;
    private int durationInSeconds;

    public PhoneNumber getOriginatingPhoneNumber() {
        return originatingPhoneNumber;
    }

    public PhoneNumber getReceivedPhoneNumber() {
        return receivedPhoneNumber;
    }

    public Date getStartTime() {
        return startTime;
    }

    public int getDurationInSeconds() {
        return durationInSeconds;
    }

    public CDR() {
    }

    public boolean isLocal() {
        return this.originatingPhoneNumber.getExtension().equalsIgnoreCase(this.receivedPhoneNumber.getExtension());
    }

    public void setOriginatingPhoneNumber(PhoneNumber originatingPhoneNumber) {
        this.originatingPhoneNumber = originatingPhoneNumber;
    }

    public void setReceivedPhoneNumber(PhoneNumber receivedPhoneNumber) {
        this.receivedPhoneNumber = receivedPhoneNumber;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setDurationInSeconds(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
