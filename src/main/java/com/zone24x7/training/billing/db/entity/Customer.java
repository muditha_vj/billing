package com.zone24x7.training.billing.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by TharinduMP on 11/12/2018.
 */
@Entity
public class Customer {

    @Id
    private int id;
    private String name;
    @OneToMany
    private List<CustomerPhone> customerPhones;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CustomerPhone> getCustomerPhones() {
        return customerPhones;
    }

    public void setCustomerPhones(List<CustomerPhone> customerPhones) {
        this.customerPhones = customerPhones;
    }
}
