package com.zone24x7.training.billing;

public interface Tax {
    double calculateTax(double billAmount);
}
